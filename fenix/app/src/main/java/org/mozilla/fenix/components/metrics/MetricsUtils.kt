/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

package org.mozilla.fenix.components.metrics

import android.content.Context
import android.util.Base64
import androidx.annotation.VisibleForTesting
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import mozilla.components.browser.state.search.SearchEngine
import mozilla.components.support.base.log.logger.Logger
import org.mozilla.fenix.GleanMetrics.Events
import org.mozilla.fenix.GleanMetrics.Metrics
import java.io.IOException
import java.security.NoSuchAlgorithmException
import java.security.spec.InvalidKeySpecException
import javax.crypto.SecretKeyFactory
import javax.crypto.spec.PBEKeySpec

object MetricsUtils {
    // The number of iterations to compute the hash. RFC 2898 suggests
    // a minimum of 1000 iterations.
    private const val PBKDF2_ITERATIONS = 1000
    private const val PBKDF2_KEY_LEN_BITS = 256

    /**
     * Possible sources for a performed search.
     */
    enum class Source {
        ACTION, SHORTCUT, SUGGESTION, TOPSITE, WIDGET, NONE
    }

    /**
     * Records the appropriate metric for performed searches.
     * @engine the engine used for searching.
     * @isDefault whether te engine is the default engine or not.
     * @searchAccessPoint the source of the search. Can be one of the values of [Source].
     */
    fun recordSearchMetrics(
        engine: SearchEngine,
        isDefault: Boolean,
        searchAccessPoint: Source,
    ) {
        val identifier = if (engine.type == SearchEngine.Type.CUSTOM) "custom" else engine.id.lowercase()
        val source = searchAccessPoint.name.lowercase()

        Metrics.searchCount["$identifier.$source"].add()

        val performedSearchExtra = if (isDefault) {
            "default.$source"
        } else {
            "shortcut.$source"
        }

        Events.performedSearch.record(Events.PerformedSearchExtra(performedSearchExtra))
    }

    /**
     * Records the appropriate metric for performed Bookmark action.
     * @param action The [BookmarkAction] being counted.
     * @param source Describes where the action was called from.
     */
    fun recordBookmarkMetrics(
        action: BookmarkAction,
        source: String,
    ) {
        when (action) {
            BookmarkAction.ADD -> Metrics.bookmarksAdd[source].add()
            BookmarkAction.EDIT -> Metrics.bookmarksEdit[source].add()
            BookmarkAction.DELETE -> Metrics.bookmarksDelete[source].add()
            BookmarkAction.OPEN -> Metrics.bookmarksOpen[source].add()
        }
    }

    /**
     * Describes which bookmark action is being recorded.
     */
    enum class BookmarkAction {
        ADD, EDIT, DELETE, OPEN
    }
}
